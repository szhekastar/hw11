const buttons = document.querySelectorAll(".btn");

const handler = function(event) {
    buttons.forEach(function(button) {
        const atributeData = button.getAttribute("data");
        // console.log(event.code);
        if(atributeData === event.code) {
            button.classList.add("btn-color");
        } else {
            button.classList.remove("btn-color");
        }
    })
}

document.addEventListener("keydown", handler);


